/*
 * This file is part of KDE Telepathy Chat
 *
 * Copyright (C) 2014 Aleix Pol Gonzalez <aleixpol@blue-systems.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

// TODO: this should be developed so that it can be eventually moved into KPeople

import QtQuick 2.3
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.0
import org.kde.people 1.0
import org.kde.kquickcontrolsaddons 2.0

MouseArea
{
    id: mouse
    property alias detailsComponent: extraInfo.sourceComponent
    property alias personUri: personData.personUri
    hoverEnabled: true

    Rectangle {
        anchors.fill: parent
        color: mouse.containsMouse ? palette.mid : palette.light
        opacity: 0.2
    }

    PersonData {
        id: personData
    }

    RowLayout {
        anchors.fill: parent

        QPixmapItem {
            Layout.preferredWidth: 40
            Layout.preferredHeight: 40

            pixmap: personData.person.photo
        }
        ColumnLayout {
            Layout.fillWidth: true
            Label {
                Layout.fillWidth: true

                anchors.right: parent.right
                text: personData.person.name
                horizontalAlignment: Text.AlignRight
                elide: Text.ElideRight
            }
            Loader {
                Layout.fillWidth: true
                id: extraInfo
            }
        }
    }
}
