/*
 * This file is part of KDE Telepathy Chat
 *
 * Copyright (C) 2015 Aleix Pol Gonzalez <aleixpol@blue-systems.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

import QtQuick 2.1
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.3
import org.kde.telepathy 0.1 as KTp

Rectangle
{
    SystemPalette { id: palette }
    color: palette.base

    property alias model: view.model
    property alias filterString: filter.text

    ColumnLayout
    {
        anchors.fill: parent
    //     TODO: uncomment when on Qt 5.5, QtQuick 2.5
    //     Shortcut {
    //         sequence: "Ctrl+F"
    //         onActivated: filter.forceActiveFocus()
    //     }

        TextField {
            id: filter
            placeholderText: i18n("Filter...")
            Layout.fillWidth: true
            onTextChanged: view.currentIndex = 0
            Keys.onUpPressed: view.currentIndex = Math.max(view.currentIndex-1, 0)
            Keys.onDownPressed: view.currentIndex = Math.min(view.currentIndex+1, view.count-1)
            onAccepted: {
                view.currentItem.startChat()
            }
        }
        ScrollView
        {
            Layout.fillWidth: true
            Layout.fillHeight: true

            signal selected(var account, var contact)

            ListView {
                id: view
                spacing: 3
                highlight: Rectangle { color: palette.mid; height: 30; width: view.width }
                currentIndex: 0

                delegate:ContactDelegate {
                        height: 40 //TODO: check font metrics?
                        width: parent.width
                        function startChat() {
                            telepathyManager.startChat(model.account, model.contact, "org.freedesktop.Telepathy.Client.KTp.Chat");
                        }
                        personUri: model.personId
                        detailsComponent: Label {
                            text: model.lastMessage
                            horizontalAlignment: Text.AlignRight
                            elide: Text.ElideRight
                        }
                        onClicked: { startChat(); }
                }
            }
        }
    }
}
