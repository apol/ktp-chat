/*
    Copyright (C) 2012  Lasath Fernando <kde@lasath.org>
    Copyright (C) 2012 David Edmundson <kde@davidedmundson.co.uk>
    Copyright (C) 2012 Aleix Pol <aleixpol@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

import QtQuick 2.1
import QtQuick.Layouts 1.1
import org.kde.telepathy 0.1
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents

FocusScope {
    id: chatWidget
    property Conversation conv

    signal closeRequested

    RowLayout {
        id: titleArea
        clip: true
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
        }
        spacing: 3
        height: 30

        PlasmaCore.IconItem {
            Layout.fillHeight: true

            source: conv.presenceIcon
        }

        PlasmaComponents.Label {
            Layout.fillHeight: true
            Layout.fillWidth: true

            text: conv.title
            elide: Text.ElideRight
        }

//         PlasmaComponents.ToolButton {
//             Layout.fillHeight: true
//             checkable: true
//             checked: pin.pinned
//
//             iconSource: "rating"
//             onClicked: pin.toggle()
//             tooltip: i18n("Pin contact")
//             ContactPin {
//                 id: pin
//                 model: pinnedModel
//                 contact: chatWidget.conv.targetContact
//                 account: chatWidget.conv.account
//             }
//         }

        PlasmaComponents.ToolButton {
            Layout.fillHeight: true
            iconSource: "view-conversation-balloon"
            tooltip: i18n("Move conversation into a window")
            onClicked: conv.delegateToProperClient()
        }

        PlasmaComponents.ToolButton {
            Layout.fillHeight: true
            iconSource: "dialog-close"
            tooltip: i18n("Close conversation")
            onClicked: chatWidget.closeRequested();
        }
    }

    ConversationView {
        id: view
        anchors {
            top: titleArea.bottom
            left: parent.left
            right: parent.right
            bottom: disconnectedLabel.top
            rightMargin: 5
            leftMargin: 5
        }
        model: conv.messages
    }

    PlasmaComponents.Label {
        id: disconnectedLabel
        visible: !conv.valid
        height: visible ? contentHeight : 0

        anchors {
            left: parent.left
            right: parent.right
            bottom: input.top
        }

        text: i18n("Chat is not connected. You cannot send messages at this time")
        wrapMode: Text.Wrap
    }

    PlasmaComponents.TextField {
        id: input
        focus: true
        enabled: conv.valid

        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        Keys.onReturnPressed: {
            view.model.sendNewMessage(text);
            text = "";
        }
    }
}
