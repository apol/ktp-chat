/*
    Copyright (C) 2012  Lasath Fernando <kde@lasath.org>
    Copyright (C) 2012 David Edmundson <kde@davidedmundson.co.uk>
    Copyright (C) 2015 Aleix Pol <aleixpol@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

import QtQuick 2.1
import QtQuick.Controls 1.1
import org.kde.telepathy 0.1

ListView {
    id: view
    property bool followConversation: true

    boundsBehavior: Flickable.StopAtBounds
    section.property: "senderAlias"
    section.delegate: Label { text: section; font.bold: true; anchors.right: parent.right}
    clip: true

    //we need this so that scrolling down to the last element works properly
    //this means that all the list is in memory
    cacheBuffer: Math.max(0, contentHeight)

    delegate: Loader {
        Component.onCompleted: {
            switch(model.type) {
                case MessagesModel.MessageTypeOutgoing:
//                             console.log("Type: MessagesModel::MessageTypeOutgoing");
                    source = "OutgoingDelegate.qml"
                    break;
                case MessagesModel.MessageTypeAction:
//                             console.log("Type: MessagesModel::MessageTypeAction");
                    source = "ActionDelegate.qml";
                    break;
                case MessagesModel.MessageTypeIncoming:
                default:
                    source = "TextDelegate.qml";
            }
        }
    }

    onMovementEnded: followConversation = atYEnd //we only follow the conversation if moved to the end

    onContentHeightChanged: {
        if(followConversation && contentHeight>height) {
            view.positionViewAtEnd()
        }
    }
}
