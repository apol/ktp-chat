/*
 * This file is part of KDE Telepathy Chat
 *
 * Copyright (C) 2014 Aleix Pol Gonzalez <aleixpol@blue-systems.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

import QtQuick 2.1
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1
import QtQuick.Window 2.2
import org.kde.telepathy 0.1 as KTp
 
ApplicationWindow
{
    id: root
    visible: true
    width: 800
    height: 600
    property int currentIndex: -1

    KTp.ConversationsModel {
        id: conversationsModel
    }

    KTp.ContactsModel {
        id: contactsModel
        accountManager: telepathyManager.accountManager
        presenceTypeFilterFlags: KTp.ContactsModel.HideAllOffline //TODO: remove
        trackUnreadMessages: true
        sortRoleString: "presenceType"
    }

    Component.onCompleted: {
        telepathyManager.addTextChatFeatures();
        telepathyManager.registerClient(conversationsModel, "KTp.Chat");
    }
    Component.onDestruction: {
        telepathyManager.unregisterClient(conversationsModel);
    }

    Loader {
        id: load
        anchors.fill: parent
        sourceComponent: (root.width/Screen.logicalPixelDensity)<80 ? smallVersion : bigVersion
    }

    Component {
        id: bigVersion
        RowLayout {
            anchors.fill: parent
            ContactList {
                Layout.fillHeight: true
                Layout.fillWidth: true
                Layout.maximumWidth: parent.width/3
                Layout.minimumWidth: 100

                model: contactsModel
                onFilterStringChanged: contactsModel.globalFilterString = filterString
            }

            ConversationDisplay {
                Layout.fillHeight: true
                Layout.fillWidth: true
            }
        }
    }

    Component {
        id: smallVersion
        StackView {
            id: stack
            initialItem: ContactList {
                model: contactsModel
                onFilterStringChanged: contactsModel.globalFilterString = filterString
            }

            Component {
                id: convoDisplay
                ConversationDisplay {
                    onCloseRequested: stack.pop()
                }
            }

            Connections {
                target: conversationsModel
                onActiveChatIndexChanged: stack.push(convoDisplay)
            }
        }
    }
}
